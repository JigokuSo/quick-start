# Quick Start #
 
Quick Start is an introduction for new users and team members to BitBucket. I am
writing this to help people quickly get starting and working. This is also an exercise
for me to better understand the system myself. If I can teach it well, it means I have
an adequate understanding.
 
## Sign Up ##

 Self explanatory. Do it. If you were invited to a team, sign up through that.
 
## Setup Git & Mercurial ##

### Download ###

[Git Download][3]

Select Git-1.8.0-preview20121022.exe as seen below and install for Windows.

![Git Installer Windows][5]

---

[Mercurial Download][4]

Select 2.6.1 install based on appropriate OS.

![Git Mercurial Installer][6]

Turns a `textarea` into a Markdown editor.
	
	test text here.
 
    Hashify.editor(id [, preview [, callback]])
 
#### Parameters ####
 
##### `id`
 
The `id` of a `textarea`, or a `textarea` node.
 
##### `preview`
 
Boolean which determines whether the "preview" link is included. Defaults to
`true`.
 
##### `callback`
 
Function to be invoked every time Hashify Editor handles an event. Within the
function, `this` refers to the `textarea`. [Hashify][4], for example, uses a
callback to update the URL with each keystroke.
 
#### Example
 
    Hashify.editor(editor, false, function () {
      setLocation(Hashify.encode(this.value));
    });
 
### `Hashify.encode`
 
Returns the Base64-encoded representation of a binary input string.
 
    Hashify.encode(text)
 
### `Hashify.decode`
 
Returns the binary representation of a Base64-encoded input string.
 
    Hashify.decode(text)
 
 
## Sites using Hashify Editor
 
  - [hashify.me][1]
  - [davidchambersdesign.com][2]
 
 
[1]: http://hashify.me/
[2]: http://davidchambersdesign.com/
[3]: http://code.google.com/p/msysgit/downloads/list "Git"
[4]: http://tortoisehg.bitbucket.org/download/index.html "Mercurial"
[5]: https://bitbucket.org/TeknoXI/quick-start/downloads/Git%20Installer.PNG "Git Installer"
[6]: https://bitbucket.org/TeknoXI/quick-start/downloads/Mercurial%20Installer.PNG "Mercurial Installer"